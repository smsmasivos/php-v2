<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$filename = getcwd()."/../xml/config.xml";

if(!empty($_POST)){
  extract($_REQUEST);

  switch($cmd){
    case "xmls":
      try {
        if (file_exists($filename)) {
          $xml = new SimpleXMLElement($filename, null, true);
          $data = $xml->config[0];
          $data->code = 1;
          echo json_encode($data);
        } else {
          $xml = new SimpleXMLElement('<xml/>');
          $xml->addChild("config");
          $xml->config->addChild("ApiKey", "_");
          $myfile = fopen($filename, "w");
          $txt = $xml->asXML();
          fwrite($myfile, $txt);
          fclose($myfile);
          $data = $xml->config[0];
          $data->code = 1;
          echo json_encode($data);        
        }
      } catch (Exception $e) {
        $thrown = new \stdClass();
        $thrown->code = 0;
        $thrown->message = $e->getMessage();
        echo json_encode($thrown);
      }
      
    break;

    case "auth":
      try {
        $xml = new SimpleXMLElement($filename, null, true);
        $xml->config[0]->ApiKey = $api;

        $myfile = fopen($filename, "w");
        $txt = $xml->asXML();
        fwrite($myfile, $txt);
        fclose($myfile);
        echo json_encode(array("success" => true, "message" => "API KEY almacenada correctamente"));
      } catch (Exception $e) {
        echo json_encode(array("success" => false, "message" => $e->getMessage()));
      }
      
      
    break;

    case "send":
      $xml = new SimpleXMLElement($filename, null, true);
      $data = $xml->config[0];

      $text = $texto;
      $number = $dest; 
      $country = $c;
      $name = $nombre;
      $sandbox = $sand;

      if($name == ""){
        $name = "Escribe un nombre para tu campaña ".date("Y-m-d H:i:s");
      }
      $params = array(
        "message" => $text,
        "numbers" => $number,
        "country_code" => $country,
        "name" => $name,
        "sandbox" => $sandbox
      );

      $headers = array(
        "apikey: ".$data->ApiKey
      );
      curl_setopt_array($ch = curl_init(), array(
        CURLOPT_URL => "https://api.smsmasivos.com.mx/sms/send",
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => http_build_query($params),
        CURLOPT_RETURNTRANSFER => 1
      ));
      $response = curl_exec($ch);
      curl_close($ch);
      echo $response;
      
    break;

    case "credits":
      $xml = new SimpleXMLElement($filename, null, true);
      $data = $xml->config[0];
    
      $headers = array(
        "apikey: ".$data->ApiKey
      );
      curl_setopt_array($ch = curl_init(), array(
        CURLOPT_URL => "https://api.smsmasivos.com.mx/credits/consult",
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => http_build_query(array()),
        CURLOPT_RETURNTRANSFER => 1
      ));
      $response = curl_exec($ch);
      curl_close($ch);
      $response = json_decode($response);

      if($response->success == true && $response->code == "report_04"){
        echo json_encode(array(1, $response->credit));
      }else{
        echo json_encode(array(0, $response->message)) ; 
      } 
    break;

    case "2fa_register":
      $xml = new SimpleXMLElement($filename, null, true);
      $data = $xml->config[0];

      $format = $formato;

      $params = array(
        "phone_number" => $dest,
        "country_code" => intval($c),
        "code_length" => intval($digitos)
      );

      if($nombre != ""){
        $params["company"] = $nombre;
      }
      
      curl_setopt_array($ch = curl_init(), array(
        CURLOPT_URL => "https://api.smsmasivos.com.mx/protected/".$format."/phones/verification/start",
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => array(
          'Content-Type: application/json',
          "apikey: ".$data->ApiKey
        ),
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => json_encode($params),
        CURLOPT_RETURNTRANSFER => 1
      ));

      $response = curl_exec($ch);
      curl_close($ch);
      
      echo $response;
    break;

    case "2fa_validate":
      $xml = new SimpleXMLElement($filename, null, true);
      $data = $xml->config[0];

      $format = $formato;

      $params = array(
        "phone_number" => $dest,
        "verification_code" => $code
      );

      curl_setopt_array($ch = curl_init(), array(
        CURLOPT_URL => "https://api.smsmasivos.com.mx/protected/".$format."/phones/verification/check",
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_HEADER => 0,
        CURLOPT_HTTPHEADER => array(
          "apikey: ".$data->ApiKey
        ),
        CURLOPT_POST => 1,
        CURLOPT_POSTFIELDS => http_build_query($params),
        CURLOPT_RETURNTRANSFER => 1
      ));
      $response = curl_exec($ch);
      curl_close($ch);
      
      echo $response;
                              
    break;
  }
}
?>